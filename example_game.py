import utils
from classes import Room, Player

prompts = {
    "cameraScene": "You see a camera that has been dropped on the ground. Someone has been here recently.",
    "showShadowFigure": "You see a dark shadowy figure appear in the distance. You are creeped out.",
    "introScene": "You are at a crossroads, and you can choose to go down any of the four hallways.",
    "hauntedRoom": "You hear strange voices. You think you have awoken some of the dead.",
    "showSkeletons": "You see a wall of skeletons as you walk into the room. Someone is watching you.",
    "strangeCreature": "A strange goul-like creature has appeared. You can either run or fight it.",

    "dead_end": "You find that this door opens into a wall."
}


def monster(player):
    options = ["flee", "fight"]
    input_text = utils.get_option_text(options)
    action = utils.guarded_input(input_text, utils.ERROR_TXT, lambda x: x in options,
                                 extra_prompt="What would you like to do?")
    if action == "flee":
        player.move("left")
        return
    if player.has("knife"):
        print("You kill the goul with the knife you found earlier.")
        print("After moving forward, you find one of the exits. Congrats!")
        exit()
    else:
        print("The goul-like creature has killed you.")
        exit()


def obtain_knife(player):
    print("You open some of the drywall to discover a knife.")
    player.get_item("knife")


def intro_text():
    print("Welcome to the Adventure Game!")
    print("As an avid traveler, you have decided to visit the Catacombs of Paris.")
    print("However, during your exploration, you find yourself lost.")
    print("You can choose to walk in multiple directions to find a way out.")


def init_game():
    dead_end = Room(prompts["dead_end"])

    exit_room = Room("You made it! You've found an exit.", call=exit, pass_player=False)
    death_room = Room("You are killed.", call=exit, pass_player=False)

    strangeCreature = Room(prompts["strangeCreature"], call=monster)
    dead_end_knife = Room(prompts["dead_end"], call=obtain_knife)

    cameraScene = Room(prompts["cameraScene"])
    showShadowFigure = Room(prompts["showShadowFigure"])
    hauntedRoom = Room(prompts["hauntedRoom"])
    introScene = Room(prompts["introScene"])
    showSkeletons = Room(prompts["showSkeletons"])

    cameraScene.set_connections(None, None, showShadowFigure, exit_room)
    showShadowFigure.set_connections(None, introScene, dead_end, cameraScene)
    hauntedRoom.set_connections(death_room, exit_room, None, introScene)
    introScene.set_connections(showShadowFigure, showSkeletons, hauntedRoom, dead_end)
    showSkeletons.set_connections(introScene, strangeCreature, None, dead_end_knife)

    return Player(introScene)


def main():
    intro_text()
    player = init_game()
    utils.game_loop(player)


if __name__ == "__main__":
    main()
