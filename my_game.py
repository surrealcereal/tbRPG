import operator
import os
import random
import threading
import time

import utils
from classes import Room, Player

prompts = {
    "first_room": "You find yourself in a central room that connects to four other rooms.",
    "code_room": "You see some ancient writing on the walls. It says: \"dog cat horse\".",
    "dead_end_sword": "You found a powerful looking sword. You take it and leave.",
    "monster_room": "You see a big silhouette in the room. You can either approach it or go back.",
    "locked_door": "This door is locked with a code.",
    "totem_room": "You obtain a mystical looking totem. It looks like it has to do with religion.",
    "puzzle_room": "You enter the room but have to solve a puzzle to progress.",
    "skeleton_room": "As soon as you enter the room, a group of skeletons awaken and attack you!",
    "ritual_room": "This room seems to be a place of ritual. You can see a pedestal for a sword and totem.",
    "trap_room": "As soon as you walk into the room, a boulder from above crushes you!",

    "dead_end": "This door leads into an empty room. You decided to return.",
    "exit_room": "You have found an exit."
}


def intro_text():
    print("Welcome to my text-based RPG!")
    print("As an adventurer, you decided to enter an unexplored Mayan temple in Mexico.")
    print("However, while exploring, you got lost.")
    print("You must find a way out alive.")
    print("Walk in the allowed directions to try to escape.")


def death():
    print("You are dead.")
    exit()


def escape():
    print("You made it out alive!")
    exit()


def obtain_sword(player):
    if not player.has("sword"):
        player.get_item("sword")
        player.location.update_prompt("This room is now empty as you have taken the sword.")


def code(player):
    input_text = utils.get_option_text(["back", "enter the password"])
    action = utils.guarded_input(input_text, "The password you entered is incorrect.",
                                 lambda x: x in ["back", "dog cat horse"],
                                 extra_prompt="Enter the password or go back.")
    if action == "back":
        player.go_back()
        return
    print("The password you entered is correct! The door leads to an exit.")
    escape()


def monster(player):
    options = ["go closer", "back"]
    input_text = utils.get_option_text(options)
    action = utils.guarded_input(input_text, utils.ERROR_TXT, lambda x: x in options,
                                 extra_prompt="What would you like to do?")
    if action == "back":
        player.go_back()
        return
    print("The silhouette turns out to be an ancient monster!")
    if player.has("sword"):
        print("You killed the monster with the sword!")
        print("The monster's dead body falls and reveals an exit.")
        escape()
    else:
        print("It kills you instantly.")
        death()


def obtain_totem(player):
    if not player.has("totem"):
        player.get_item("totem")
        player.location.update_prompt("You see the empty pedestal on which the totem stood.")


def puzzle():
    def timer(solved):
        for i in range(15):
            if solved.is_set():
                exit()
            time.sleep(1)
        print("\nThe temple collapsed onto you as you could not solve the question in time!")
        # Calling death would exit the thread, we need to terminate the whole script through os._exit.
        print("You are dead.")
        os._exit(0)

    print("You are presented with a math problem!")
    print("You have 15 seconds to solve it.")
    solved = threading.Event()
    t = threading.Thread(target=timer, args=(solved,))
    t.start()
    x = random.randint(0, 100)
    y = random.randint(0, 100)
    func = random.choice((operator.add, operator.sub, operator.mul, operator.truediv))
    result = func(x, y)
    symbols = {
        operator.add: "+",
        operator.sub: "-",
        operator.mul: "*",
        operator.truediv: "/"
    }
    problem_str = f"{x} {symbols[func]} {y}"
    # not int(x) as "asd" would raise ValueError, so str(result)
    utils.guarded_input(f"The answer to {problem_str} is: ", "The answer you have given is incorrect!",
                        lambda a: a == str(result))
    solved.set()
    print("You solved the puzzle correctly.")
    print("Solving the puzzle unlocked a secret exit!")
    escape()


def skeletons(player):
    if player.has("bone"):
        return
    if not player.has("sword"):
        print("The skeletons swarmed and killed you.")
        death()
    else:
        print("You killed the skeletons with the sword.")
        player.get_item("bone")
        player.location.update_prompt("You see the bones of the skeletons you have slain.")


def ritual(player):
    if player.has("totem") and player.has("sword"):
        print("You place the totem and the sword onto the pedestal.")
        print("You have been teleported out of the temple by magic!")
        escape()
    else:
        print("You return as you see nothing else useful there.")


def init_game():
    dead_end = Room(prompts["dead_end"])
    dead_end_sword = Room(prompts["dead_end_sword"], call=obtain_sword)

    exit_room = Room(prompts["exit_room"], call=escape, pass_player=False)
    trap_room = Room(prompts["trap_room"], call=death, pass_player=False)

    first_room = Room(prompts["first_room"])
    code_room = Room(prompts["code_room"])

    monster_room = Room(prompts["monster_room"], call=monster)
    locked_door = Room(prompts["locked_door"], call=code)
    totem_room = Room(prompts["totem_room"], call=obtain_totem)
    puzzle_room = Room(prompts["puzzle_room"], call=puzzle, pass_player=False)
    skeleton_room = Room(prompts["skeleton_room"], call=skeletons)
    ritual_room = Room(prompts["ritual_room"], call=ritual)

    first_room.set_connections(ritual_room, skeleton_room, totem_room, code_room)
    code_room.set_connections(dead_end_sword, monster_room, first_room, dead_end)
    skeleton_room.set_connections(first_room, trap_room, None, exit_room)
    totem_room.set_connections(puzzle_room, locked_door, dead_end, first_room)

    return Player(first_room)


def main():
    intro_text()
    player = init_game()
    utils.game_loop(player)


if __name__ == "__main__":
    main()
