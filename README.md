# tbRPG
A simple **t**ext-**b**ased **RPG** written in Python as homework.

`example_game.py` reimplements the example game (from [makeuseof.com](https://www.makeuseof.com/python-text-adventure-game-create/)) with classes to reduce repeated code and decrease the time it takes to implement the basics of dungeon navigation.

`my_game.py` is the game I actually am submitting for the assignment that has a few added suprises above the example's mechanics.

## To run it:
```
git clone https://codeberg.org/surrealcereal/tbRPG
cd tbRPG 
```
and then, to run `example_game.py`:
```
python example_game.py
```
and, to run `my_game.py`:
```
python my_game.py
```

## Diagram of the Dungeon Implemented in `my_game.py`
![diagram](https://codeberg.org/surrealcereal/tbRPG/raw/branch/main/diagram.png)
