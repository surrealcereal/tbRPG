def guarded_input(input_prompt, error_text, condition, extra_prompt=None, standardize=True):
    def get_input(input_prompt):
        if extra_prompt:
            print(extra_prompt)
        ret = input(input_prompt)
        return ret.lower() if standardize else ret

    while not condition((ret := get_input(input_prompt))):
        print(error_text)
    return ret


def get_option_text(options):
    return f"Options: {'/'.join(options)} "


ERROR_TXT = "Please enter a valid option."


def game_loop(player):
    while True:
        print(player.location.prompt)
        player.location.do_call(player)
        connections = player.location.connections
        if connections:
            input_text = get_option_text(connections)
            direction = guarded_input(input_text, ERROR_TXT, lambda x: x in connections,
                                            extra_prompt="Where would you like to go?")
            player.move(direction)
        else:
            player.go_back()
