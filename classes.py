import utils


class Player:
    def __init__(self, location):
        self.name = utils.guarded_input("Let's start with your name: ", "That doesn't seem right.", lambda x: bool(x),
                                        standardize=False)
        print(f"Good luck, {self.name}.")
        self.inventory = dict()
        self.location = location

        self.last_location = None

    def get_item(self, item):
        self.inventory[item] = True

    def has(self, item):
        return item in self.inventory

    def move(self, direction):
        self.last_location = self.location
        self.location = self.location.get_room(direction)

    def go_back(self):
        self.location = self.last_location


class Room:
    def __init__(self, prompt, call=None, pass_player=True):
        self.left = None
        self.right = None
        self.back = None
        self.forward = None
        self.prompt = prompt

        self.call = call
        self.pass_player = pass_player

        self.connections = None

    def set_connections(self, left, right, back, forward):
        args = locals()
        del args["self"]
        for v, k in args.items():
            setattr(self, v, k)
        self.connections = [v for v, k in args.items() if k]

    def get_room(self, direction):
        return getattr(self, direction)

    def do_call(self, player):
        try:
            if not self.call:
                return
            if self.pass_player:
                self.call(player)
            else:
                self.call()
        except AttributeError:
            pass

    def update_prompt(self, new_prompt):
        self.prompt = new_prompt
